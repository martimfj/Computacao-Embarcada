/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>

#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

/* RTC */
#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      59
#define SECOND      0

/* LED */
#define LED_PIO PIOC
#define LED_PIO_ID ID_PIOC
#define LED_PIN 30
#define LED_PIN_MASK (1 << LED_PIN)

/* Botões */
#define BUT1_PIO PIOD
#define BUT1_PIO_ID ID_PIOD
#define BUT1_PIN 28
#define BUT1_PIN_MASK (1 << BUT1_PIN)

#define BUT2_PIO PIOC
#define BUT2_PIO_ID	ID_PIOC
#define BUT2_PIN 31
#define BUT2_PIN_MASK (1 << BUT2_PIN)

#define BUT3_PIO PIOA
#define BUT3_PIO_ID	ID_PIOA
#define BUT3_PIN 19
#define BUT3_PIN_MASK (1 << BUT3_PIN)

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t flag_led = 1;
volatile uint8_t flag_alarm = 0;
volatile uint8_t flag_screen = 1; //1: Primeira, 0:Segunda

volatile uint8_t flag_but1_1 = 0;
volatile uint8_t flag_but2_1 = 0;
volatile uint8_t flag_but3_1 = 0;

volatile uint8_t flag_but1_2 = 0;
volatile uint8_t flag_but2_2 = 0;
volatile uint8_t flag_but3_2 = 0;

uint32_t minuto_lcd = 0;
uint32_t alarm_counter = 0;

char clock_buffer[100];
char alarm_buffer[100];

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void BUTs_init(void);
void LEDs_init(void);
void TC_init(Tc *TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init(void);
void RTC_Handler(void);
void pin_toggle(Pio *pio, uint32_t mask);

void pin_toggle(Pio *pio, uint32_t mask){
	if (pio_get_output_data_status(pio, mask)){
		pio_clear(pio, mask);
	}
	else{
		pio_set(pio, mask);
	}
}

void TC0_Handler(void){
	volatile uint32_t ul_dummy;
	ul_dummy = tc_get_status(TC0, 0);

	UNUSED(ul_dummy);

	if(flag_led == 0){
		pin_toggle(LED_PIO, LED_PIN_MASK);
	}
}

/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc *TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	/* Configura o PMC */
	/* TC0 : ID_TC0, ID_TC1, ID_TC2
	   TC1 : ID_TC3, ID_TC4, ID_TC5
	   TC2 : ID_TC6, ID_TC7, ID_TC8  */

	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  freq Mhz e interrupco no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrupco no TC canal 0 */
	/* Interrupo no C */
	NVIC_EnableIRQ((IRQn_Type)ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

//Interrupcao dos Botões
static void button1_handler(uint32_t id, uint32_t mask){
	if(flag_screen){
		flag_but1_1 =! flag_but1_1;
	}
	else{
		flag_but1_2 =! flag_but1_2;
	}
}

static void button2_handler(uint32_t id, uint32_t mask){
	if(flag_screen){
		flag_but2_1 =! flag_but2_1;
	}
	else{
		flag_but2_2 =! flag_but2_2;
	}
}

static void button3_handler(uint32_t id, uint32_t mask){
	if(flag_screen){
		flag_but3_1 =! flag_but3_1;
	}
	else{
		flag_but3_2 =! flag_but3_2;
	}
}

void clearDisplay(void){
	gfx_mono_draw_filled_rect(0, 0, 128, 32, GFX_PIXEL_CLR);
}

void displayTime(void){
	gfx_mono_draw_string(clock_buffer, 90, 11, &sysfont);
}

void updateTime(uint32_t h, uint32_t m){
	if(h < 10 && m < 10){
		sprintf(clock_buffer, "0%d:0%d", h, m);
	}
	else if(h < 10 && m > 10){
		sprintf(clock_buffer, "0%d:%d", h, m);
	}
	else if (h > 10 && m < 10){
		sprintf(clock_buffer, "%d:0%d", h, m);
	}
	else{
		sprintf(clock_buffer, "%d:%d", h, m);
	}
	displayTime();
}

void displayCounter(void){
	gfx_mono_draw_string(alarm_buffer, 90, 11, &sysfont);
}

void updateCounter(int alarm_counter){
	sprintf(alarm_buffer, "%d", alarm_counter);
	displayCounter();
}

void setDisplay(void){
	if(flag_screen){
		clearDisplay();
		gfx_mono_draw_filled_circle(4, 2, 2, GFX_PIXEL_SET, GFX_WHOLE);
		gfx_mono_draw_string("Alarm Config", 10, 0, &sysfont);

		gfx_mono_draw_circle(4, 13, 2, GFX_PIXEL_SET, GFX_WHOLE);

		if(flag_alarm == 0){
			gfx_mono_draw_filled_circle(4, 25, 2, GFX_PIXEL_SET, GFX_WHOLE);
			gfx_mono_draw_string("Alarm OFF", 10, 22, &sysfont);
		}

		if(flag_alarm == 1){
			char display_buffer[150];
			sprintf(display_buffer, "Alarm ON (%d)", alarm_counter);
			gfx_mono_draw_filled_circle(4, 25, 2, GFX_PIXEL_SET, GFX_WHOLE);
			gfx_mono_draw_string(display_buffer, 10, 22, &sysfont);
		}

		if (flag_alarm == 2){
			gfx_mono_draw_filled_circle(4, 25, 2, GFX_PIXEL_SET, GFX_WHOLE);
			gfx_mono_draw_string("Ringing", 10, 22, &sysfont);
		}

		displayTime();
	}
	else{
		clearDisplay();
		gfx_mono_draw_filled_circle(4, 2, 2, GFX_PIXEL_SET, GFX_WHOLE);
		gfx_mono_draw_string("- (less)", 10, 0, &sysfont);

		gfx_mono_draw_filled_circle(4, 13, 2, GFX_PIXEL_SET, GFX_WHOLE);
		gfx_mono_draw_string("ON", 10, 11, &sysfont);

		gfx_mono_draw_filled_circle(4, 25, 2, GFX_PIXEL_SET, GFX_WHOLE);
		gfx_mono_draw_string("+ (more)", 10, 22, &sysfont);

		displayCounter();
	}
}

void activateAlarm(void){
	flag_alarm = 1;
	uint32_t h, m, s;
	rtc_get_time(RTC, &h, &m, &s);

	if (m + alarm_counter >= 60){
		rtc_set_time_alarm(RTC, 1, h + 1, 1, m + alarm_counter - 60, 1, 0);
	}

	else{
		rtc_set_time_alarm(RTC, 1, h, 1, m + alarm_counter, 1, 0);
	}
}

void disableAlarm(void){
	flag_alarm = 0;
	flag_led = 1;
	pmc_disable_periph_clk(ID_TC0);
}

void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC, RTC_IER_ALREN | RTC_IER_SECEN);
}

void RTC_Handler(void){
	uint32_t ul_status = rtc_get_status(RTC);

	if((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		
		uint32_t h, m, s;
		rtc_get_time(RTC, &h, &m, &s);
		if(minuto_lcd != m){
			updateTime(h, m);
			minuto_lcd = m;
		}
	}
	
	/* Time or date alarm */
	if((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		flag_alarm = 2;
		flag_led = 0;
		setDisplay();

		pmc_enable_periph_clk(ID_TC0);
	}
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}

void BUTs_init(void){
	//Botão 1
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, button1_handler);
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);

	//Botão 2
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, button2_handler);
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 1);

	//Botão 3
	pmc_enable_periph_clk(BUT3_PIO_ID);
	pio_set_input(BUT3_PIO, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE, button3_handler);
	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 1);
}

void LEDs_init(){
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, 1, 0, 0);
}

int main (void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	board_init();
	delay_init();
	gfx_mono_ssd1306_init();

	BUTs_init();
	LEDs_init();
	RTC_init();

	TC_init(TC0, ID_TC0, 0, 4);
	pmc_disable_periph_clk(ID_TC0);

	//128 x 32
	setDisplay();
	sprintf(alarm_buffer, "%d", alarm_counter);

	flag_led = 1;
	flag_alarm = 0;
	flag_but1_1 = 0;
	flag_but2_1 = 0;
	flag_but3_1 = 0;
	flag_but1_2 = 0;
	flag_but2_2 = 0;
	flag_but3_2 = 0;
	flag_screen = 1;
	alarm_counter = 0;
	
	while(1) {
		/* Entrar em modo sleep */
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		
		if(flag_but1_1){
			flag_but1_1 = 0;
			flag_screen = 0;
			setDisplay();
		}

		if (flag_but3_1){
			flag_but3_1 = 0;
			if(flag_alarm = 2){
				disableAlarm();
				setDisplay();
			}
		}

		if(flag_but1_2){
			flag_but1_2 = 0;
			alarm_counter -= 1;
			updateCounter(alarm_counter);
		}
		if(flag_but3_2){
			flag_but3_2 = 0;
			alarm_counter += 1;
			updateCounter(alarm_counter);
		}
		if(flag_but2_2){
			flag_but2_2 = 0;
			activateAlarm();
			flag_screen = 1;
			setDisplay();
		}
	}
}
