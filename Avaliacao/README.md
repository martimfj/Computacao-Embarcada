# Avaliação Intermediária

### Questão 1:
Dado que o módulo do motor será ligado no EXT2 da placa Atmel SAM E70, o pino/PIO que será utilizado para ligar e desligar o motor será o PA6.

### Questão 2:
#### Diagrama de Blocos:
![alt text](Diagrama_de_Blocos.png)

#### Funcionamento:
O relógio funciona em duas telas. Na primeira tela, é mostrado o relógio com a hora atual, a sinalização do status do alarme (se está configurado, se está tocando ou se está desligado) e a indicação de qual botão clicar para ir para segunda tela, de configuração do alarme.

Na segunda tela, há a indicação do que cada botão faz. O botão 1 diminui o Alarm Counter, o botão 3 aumenta o Alarm Counter e o botão 2 define o valor do Alarm Counter como o valor do Timer.
