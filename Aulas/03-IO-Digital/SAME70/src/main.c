#include "asf.h"

#define LED_PIO
#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

int main(void){
	
	// Initialize the board clock
	sysclk_init();
	
	// Disativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	// Ativa  o periferico responsavel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	
	// Inicializa PC8 como sada
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);

	while (1) {
		for (int i = 0; i<5; i++){
				
			// Apaga LED
			pio_set(LED_PIO, LED_PIO_PIN_MASK);
			delay_ms(3000);
				
			// Acende LED
			pio_clear(LED_PIO, LED_PIO_PIN_MASK);
			delay_ms(3000);
		}
		pio_set(LED_PIO, LED_PIO_PIN_MASK);
	}
	return 0;

}

